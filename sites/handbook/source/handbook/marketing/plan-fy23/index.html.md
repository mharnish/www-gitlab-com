---
layout: handbook-page-toc
title: FY23 Marketing Plan
description: FY23 Marketing Plan
twitter_image: /images/tweets/handbook-marketing.png
twitter_site: '@gitlab'
twitter_creator: '@gitlab'
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## FY23 Marketing GTM Overview
{: #overview .gitlab-purple}

<!-- DO NOT CHANGE ANCHOR -->
The purpose of this handbook page is to align our marketing team on a single FY23 marketing plan, and to communicate our FY23 approach to Sales and beyond. This page and the FY23 plan are intended to be an ongoing work-in-progress, contributed to by the entire team throughout the year to foster collaboration and focus.

Everyone can contribute. See something you'd like to discuss or iterate on? Start an MR and share it in the [marketing slack channel](https://gitlab.slack.com/archives/C0AKZRSQ5). Tag `@jgragnola` and `@lblanchard` for assistance with merging if you do not have access!

### Marketing Objectives
{: #objectives}

1. Build category leadership for the DevOps Platform
1. Deliver first order pipeline (volume and LAM)
1. Increase user engagement
1. Increase team member community and sense of belonging

To view Overview Slides (Internal only), search your [Google Drive](https://drive.google.com/drive/my-drive) for `Marketing Strategy FY23`.

### Marketing Goals
{: #goals}
1. Simplify motions and increase collaboration with our solution based marketing positioning
1. Continue to drive levers identified in Project Raptor, including:
    - Prioritize Inbound Inquiries
    - Target Strategic Accounts
    - Specialize Inbound and Outbound
    - Work on category creation
1. Also focus on FY22 areas of improvement:
    - Improve Inquiry to SAO conversion rates
    - Increase Free.com and open source conversion
    - Scale ABM process
1. Align with sales on core GTM motions
1. Build partners into our marketing motions
1. Help develop marketing team-members with training and mentoring

**Timeline:** Ensure marketing alignment by end of February 2022

### Tracking Performance
{: #performance}

On a monthly cadence, the **Marketing Key Review** will be used as the venue for tracking where we are year-to-date on the agreed upon plan.


### Roles & Responsibilities
{: #roles-responsibilities}
<!-- DO NOT CHANGE ANCHOR -->

* **Product Marketing:** Responsible for the Solution GTM Strategy including narrative, messaging, and positioning. DRI for value plays. Marketing DRI for Sales and SDR enablement. DRI for buyer and influencer personas. Responsible for fostering participation of the entire team in the research, positioning, messaging, general strategy.
* **Marketing Campaigns:** DRI for marketing campaigns. Responsible for the production of lead generation campaigns and rallying relevant teams and team members to deliver integrated marketing plans.
* **Technical Marketing:** Responsible for technical narrative and value proposition. DRI for practitioner personas. DRI for Customers Success enablement including SAs, TAMs, and Support. Responsible for technical content production such as demos, blogs, workshops, webinars, etc.
* **Content Marketing:** Responsible for content strategy and production related to the GTM motions and campaigns, and collaborating with other teams on prescriptive buyer journeys.
* **Digital Marketing:** Responsible for digital strategy and defining ideal marketing channels to reach goals.
* **Brand:** Responsible for cohesive branded journey across all tactics and between GTM Motions and campaigns.
* **Field Enablement:** Sales DRI for sales enablement and training, and collaborating with GTM core teams on launch timeline.
* **Sales Leadership:** Responsible for providing insight into the prioritization of value plays.

## Glossary of terminology
{: #glossary .gitlab-purple}

- **[GTM Motion](/handbook/marketing/plan-fy22#gtm-motions)**: a set of sales and marketing activities and asssets centered around a [solution](/handbook/marketing/strategic-marketing/usecase-gtm/).
- **[Campaign Core Team](/handbook/marketing/plan-fy23#campaign-core-teams)**: a group of marketers as functional DRIs for specific campaigns, led by marketing campaigns
- **[Integrated Campaign](/handbook/marketing/campaigns/#overview)**: managed by Campaign Managers, these are global campaigns that are leveraged across marketing and sales to drive pipeline.
* **[Key Account Lists)](/handbook/marketing/account-based-marketing/key-account-lists)**: key accounts determined by sales that will be a focus for integrated ABM campaigns.
- **[LAM]()**: Landed Addressable Market; annual revenue opportunity of the entirety of GitLab's market within our current customer base
- **[ICP](/handbook/marketing/account-based-marketing/ideal-customer-profile/)**: Ideal Customer Profile; description of our "perfect" customer company, taking into consideration firmographic, environmental and additional factors to develop our focus list of highest value accounts

## GTM Motions & Campaigns
{: #gtm-motions .gitlab-purple}
<!-- DO NOT CHANGE ANCHOR -->
In FY23, we will introcude new **solution-focused campaigns** aimed at the executive level persona, while continuing our existing **capabilities-focused** campaign approach for the individual contributor level persona. 

We will continue our always-on and continually-optimized capabilities-focused campaigns with a deep focus on DevOps Platform. CI/CD, GitOps, and DevSecOps are capabilities-focused campaigns that will also be continued, in addition to our compete campaigns.

To view a diagram of Business Initiatives > Solutions > Capabilities, go to your Google Drive and search `Maturity Model`. This is only accessible by GitLab team members. This is owned and managed by Portfolio Marketing. If you have questions about the maturity model, please direct them to the [#portfolio-marketing slack channel](https://gitlab.slack.com/archives/CPTKGRXHP).

View this [MURAL](https://app.mural.co/t/gitlab2474/m/gitlab2474/1619125370999/270c2d5df5a535223c053f2dba0d06833f9b92d1?sender=jgragnola2053) for a visual of our campaigns and how they lead to activation strategies by segment/geo. These are owned by Marketing Campaigns. If you have any questions, please direct them to the [#marketing-campaigns slack channel](https://gitlab.slack.com/archives/CCWUCP4MS).

## Campaign Core Teams & Campaign Bundles
{: #campaign-core-teams .gitlab-purple}
<!-- DO NOT CHANGE ANCHOR -->
A campaign core team is comprised of teams across marketing and revenue programs, working togehter to build new campaigns and optimize ongoing campaigns. [Learn more here >>](/handbook/marketing/campaigns/#campaign-core-teams)

When a new campaign is being developed, the campaign core team will work together to develop a campaign bundle, which is then leveraged by all teams across marketing and sales for a cohesive approach from top funnel tactics to sales conversations. [Learn more here >>](/handbook/marketing/campaigns/#campaign-bundles)

## Value Plays
{: #value-plays .gitlab-purple}
<!-- DO NOT CHANGE ANCHOR -->
A value play's intended audience is sales (SALs, SAs, TAMs, SDR as applicable) to arm the sales teams as a part the outbound motion. In some cases, there will also be inbound marketing campaign component. The playbook prepares the sales person with both prescriptive actions to take as part of the outbound motion, as well as how to respond in the case of a supporting inbound marketing campaign.

*Note: this section to be updated in conversation with Allison Gleisner, or point instead to her updated handbook page(s).*

The Value Play should include (at minimum):  
- Context (promise/offer)
- Who to meet with
- What to say
- What to show/info to use
- How to measure progress

### Beyond the GTM Motion Core Teams
{: #beyond-core-teams}

<!-- DO NOT CHANGE ANCHOR -->
**All teams play an _INTEGRAL_ role in reaching our marketing goals, regardless if they are on the Core Team or not.** Differences in team alignment (i.e. to segment/region) and approaches that will span and leverage all GTM Motions, are two reasons for teams being beyond the Core Teams.

If your team or name is not listed as part of the core team, don't worry! As part of this FY22 Marketing Plan, a keen focus is alignment and ensuring everyone across marketing is connected and collaborating. **If you would like to participate on a Core Team, you may do so - connect with the Core Team of interest to learn more about getting involved!** Anyone joining a Core Team will be expected to be an active contributor and participant.

#### Slack Channels
{: #slack}
<!-- DO NOT CHANGE ANCHOR -->

- [#marketing](https://gitlab.slack.com/archives/C01KWFD84EA)
- [#gtm-ci-cd](https://gitlab.slack.com/archives/C01DF0YMKCP)
- [#gtm-devops-platform](https://gitlab.slack.com/archives/C01NLEXE34L)
- [#gtm-gitops](https://gitlab.slack.com/archives/C0119FNPA84)
- [#gtm-devsecops](https://gitlab.slack.com/archives/C039VDJ41GE)
* [#gtm-autosd](https://gitlab.slack.com/archives/C039Z8DK2JD)

## SSoT All-Marketing Calendar
{: #marketing-calendar .gitlab-purple}
<!-- DO NOT CHANGE ANCHOR -->

The following is a single all-marketing calendar where everyone can contribute, and we can answer key questions (using filters) related to upcoming marketing plans. The calendar also lives at the top of the [Marketing Handbook Page](/handbook/marketing/#marketing-calendar).

In your [Google Drive](https://drive.google.com/drive/u/1/my-drive), search for (and bookmark) `FY23 All-Marketing Calendar SSoT`.**

This is where all teams will input and categorize planned activities, content production, themes, and more - with filters that align to key pieces of information (GTM Motions, Alliance Partners, Sales Segments, Regions, Funnel Stage, Language, and more).

## Project Management
{: #project-management .gitlab-purple}

<!-- DO NOT CHANGE ANCHOR -->
Each GTM campaign will each have an epic, where all teams can reference plans and the issues (i.e. action items) to accomplish the plans. We will be making use of 2 week milestones (Marketing - 2022-MM-DD). The Cammpaign Core Teams themselves will decide how often to have stand-ups, whether live or through slack, as well as any other synchronous calls. See more on the [Campaigns handbook page >>](/handbook/marketing/campaigns/)
