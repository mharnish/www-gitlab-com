---
layout: handbook-page-toc
title: "Executing decisions — Managing so Everyone Can Contribute (MECC)"
canonical_path: "/handbook/mecc/"
description: Executing decisions — Managing so Everyone Can Contribute (MECC)
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

{::options parse_block_html="true" /}

<%= partial("handbook/mecc/_mecc_overview.erb") %>

Executing decisions is a key output of [MECC](/handbook/mecc/). Prior tenets enable an organization to make more [informed decisions](/handbook/mecc/informed-decisions/), [more quicky](/handbook/mecc/fast-decisions/), and [more frequently](/handbook/mecc/many-decisions/). However, the usefulness of that flow hinges on an organization's ability to actually *execute* those decisions. 

MECC empowers everyone to contribute to successful execution, not just senior leaders. It also recognizes that execution is not a one-time event; rather, it is the establishment of a new baseline on which future [iterations](/handbook/values/#iteration) are applied. 

## No matrix organization

Conventional management philosophies may focus on minimizing the shortcomings of matrix organizations (or, "dotted line" reporting structures). MECC asserts that a [no matrix organization](/handbook/leadership/no-matrix-organization/) is not only feasible, but *essential* to executing decisions. By ensuring that each individual reports to exactly one other individual, contributions have a clear path to receiving feedback. 

**Here's an example**: Dual Reporting to Force Collaboration

*In a conventional organization*: A team member specializing in accounting is asked to "dual report" to the firm's chief financial officer as well as a secondary leader (e.g. VP of Sales, or VP of Development). Contribution flow from the aforementioned team member is squeezed, as each proposal must be reviewed by multiple managers. The team member is assigned two sets of goals from two differing vantage points, which forces contributions to be contorted to strike balance between both. Dual reporting may be used to force collaboration, but this approach is suboptimal.

*In an organization empowered by MECC:* A team member specializing in accounting reports to exactly one person with domain knowledge. This manager understands their day-to-day tasks, has served in that role prior, is positioned to share a single set of goals, and is suited to coach the team member through a mutually understood career path. Rather than relying on reporting structure to ensure collaboration, [collaboration is installed at a foundational, organization-wide level](/handbook/values/#collaboration). Each function is expected to honor the associated operating principles. Whenever there is need to work on a specific, high-level, cross functional business problem, assemble a [working group](/company/team/structure/working-groups/) only for the duration of time required to execute.

## Stable counterparts

In a [stable counterparts model](/blog/2018/10/16/an-ode-to-stable-counterparts/) for enabling cross-functional execution, every functional team (e.g. Support) works with the same team members from a different functional team (e.g. Development). As a member of one function, you always know who your partner in another function will be. Stable counterparts are an intentionally chosen structure designed to execute on decisions. They foster collaboration *across functions* by giving people stable counterparts for other functions they need to work with to execute decisions. This enables more social trust and familiarity, which [speeds up decision making](/handbook/mecc/fast-decisions/), facilitates [stronger communication flows](/handbook/mecc/informed-decisions/), and reduces the risk of conflicts. Stable counterparts enhance cross-functional execution without the downsides of a [matrix organization](/handbook/leadership/#no-matrix-organization).

**Here's an example**: Technical Support

*In a conventional organization*: Team members responsible for technical support may be assigned support tickets on the basis of availability rather than product, group, or function. While this approach may provide a broader view of issues and greater exposure to colleagues, the lack of stability leads to unnecessary context switching and transactional relationships. Members of this team are advised to work with an esoteric entity (e.g. "Work with Development on this.") instead of a specific individual (e.g. "Work with Jane in Development on this as your stable counterpart.") 

*In an organization empowered by MECC:* Support team members are [assigned a permanent contact](/handbook/support/support-stable-counterparts.html) for a GitLab team member within another function in the company. The ability to build long-term relationships is the foundational benefit of having stable counterparts. Repeated interactions helps us understand personal workflows and communication styles, so we know how to most effectively execute decisions with our counterparts.

## Transparent Key Performance Indicators (KPIs) and Objectives and Key Results (OKRs)

Conventional management philosophies glorify metrics, which is a nonspecific term. MECC prefers [Key Performance Indicators (KPIs)](/company/kpis/), which may be linked to [Objectives and Key Results](/company/okrs/) (OKRs) and offer greater context on their relevance to a function or the entire company. 

Crucially, KPIs for each function are [transparently shared](/handbook/values/#findability) across the organization. This enables everyone to contribute by creating visibility between departments.

**Here's an example**: Social Media Followers

*In a conventional organization*: Increasing social media followers would be purely the responsibility of the social media team, or the corporate marketing function. The metric may or may not be created in a silo. Strategies to increase social media followers would largely hinge on resources available to the corporate marketing team, and would not necessarily take into account how growing social media followers could *also* benefit other company KPIs/metrics.

*In an organization empowered by MECC:* This KPI is [shared publicly](/handbook/values/#transparency) with the entire organization. This enables members of other functions to think strategically about amplifying their work in a manner which achieves their KPIs in addition to the Social Media Followers KPI. It creates an atmosphere where collaboration is the default. Everyone is empowered to see what performance indicators matter to everyone else, and in turn, are empowered to contribute to that success even if it's in a different department. 

## Iteration is execution

Execution is not binary. It is not a one-time event. MECC reframes execution as an ongoing series of [iterations](/handbook/values/#iteration), with each one worthy of celebration. This encourages [smaller steps](/handbook/values/#move-fast-by-shipping-the-minimal-viable-change), which are more amenable to feedback and course correction. By breaking decisions down into manageable components, execution is more feasible. 

**Here's an example**: Shipping a new product feature

*In a conventional organization*: A product feature is envisioned and scoped. Success is binary; it either exists in the world, or it doesn't. When unforseen events occur (budgets shift, teams are reorganized, additional requirements are discovered, etc.), execution is impaired. 

*In an organization empowered by MECC*: A product feature is envisioned and scoped. When unforseen events occur, everyone is empowered to contribute proposals for smaller, alternative iterations that can be executed. This may mean scope is reduced or an [imperfect feature](/handbook/values/#under-construction) is released to meet a promised [due date](/handbook/values/#set-a-due-date), with future iterations aimed at achiving the [initial ambition](/handbook/values/#ambitious). 

## Single source of truth (SSoT)

Execution is *hampered* by duplicate sources of truth and *hastened* by having a [single source of truth](/handbook/values/#single-source-of-truth). MECC empowers everyone to contribute status updates and improvements within an agreed-upon documentation system (the [handbook](/handbook/handbook-usage/) at GitLab; mediums such as [Almanac](https://almanac.io/), [Guru](https://www.getguru.com/), and [Notion](https://www.notion.so/) for other organizations). Honoring the single source of truth begins at the earliest stage of execution, as outlined below. 

**Here's an example**: Agenda for Project Andromeda

*In a conventional organization*: In a cross-functional project requiring a cascading series of executions, an organization would create an agenda for each moment in time for Project Andromeda. There's no guarantee that any of the bespoke agendas will be connected in a [single source of truth](/company/culture/all-remote/handbook-first-documentation/). E.g. `Agenda for Project Andromeda Kick-off`. By creating new agendas for each touch point, context is lost and execution is made more difficult.

*In an organization empowered by MECC*: Everyone is able to contribute to the overall project, as there is one agenda for the entire project (or, at least, one agenda per component, e.g. `Project Andromeda Social Media Amplification Agenda` and `Project Andromeda Go-To-Market Agenda`). With an evergreen agenda, someone can join mid-project and read up on what has happened prior. Those who reside in various time zones can also read and contribute proposals without ever attending a synchronous meeting. With each new synchronous meeting on the topic, the same evergreen agenda doc is attached to each calendar instance. 

## Prioritize due dates over scope

A key attribute of GitLab's iteration value, ["set a due date"](/handbook/values/#set-a-due-date), enables teams to execute on decisions rather than postpone, defer, or lose momentum. MECC requires due dates not as a means to create unnecessary rigidity, but to force mechanisms that lead to [deeper trust](/handbook/leadership/building-trust/) and additional feedback. This philosophy's success hinges on an organization's ability to cut scope in order to meet due dates, enabling everyone to contribute to *future* progress on adding features in a future iteration (or, future *execution*).

**Here's an example**: Setting a due date for a brand refresh

*In a conventional organization*: If a due date is set for an overall brand refresh, and only 75% of objectives are met at the due date, the organization will most likely push the due date back. This limits who can contribute, as team members have planned their future productivity cycles and committed to future promises on the assumption that the brand refresh would be implemented by the due date. 

*In an organization empowered by MECC*: An established due date creates milestones (or gates) along the way. The DRI evaluates the pace of progress at each milestone/gate, and cuts scope along the way if necessary in order to maintain trust that the due date will be achieved. Elements which are cut are documented immediately as future iterations, enabling everyone to proactively plan future cycles *or* actively [choose to focus their time and attention](/handbook/leadership/#managers-of-one) on other work aligned to [OKRs](/company/okrs/). 

---

Executing decisions is an outcome of MECC. Key tenets which enable this outcome are linked from the [MECC homepage](/handbook/mecc/). 
