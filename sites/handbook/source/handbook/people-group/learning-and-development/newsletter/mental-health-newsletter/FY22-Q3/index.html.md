---
layout: handbook-page-toc
title: FY22-Q3 L&D Mental Health Newsletter
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


Thanks for reading the 4th edition of the Mental Health newsletter from the GitLab L&D team!

This edition of the newsletter also features a visual highlight newsletter that was shared with team members in Slack.

![newsletter highlights image](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q3/newsletter-image.png){: .medium.center}


## GitLab Resource Feature

Have you heard of Modern Health benefits offered to GitLab team members? Check out the [Modern Health handbook page](/handbook/total-rewards/benefits/modern-health/#what-does-modern-health-offer) if you're not aware of the benefit details!

Modern health for team members includes:

1. Personalized Plan: Take a well-being assessment and review which tools may be most helpful for you.
1. Professional Support including 5 coaching sessions
1. 6 therapy sessions
1. A learning library with resources about preventing burnout, managing stress, and coping with anxiety or depression based on our evidence-based digital programs.
1. Live community sessions called Circles led by Modern Health therapists and coaches

If you have questions about Modern Health, post in the #people-connect Slack channel! 



## Taking time for mental health

2021-10-10 was World Mental Health day. Spring Health shared a one-page guide called [Supporting Mental Health in an Unequal World](https://drive.google.com/file/d/12tytq8qMp4TO4DovKTkrO4ZtNkfvDNbx/view?usp=sharing) that shares data about the inequeties of mental health and health care around the globe, and what you can do to help remove stigmas about mental health.

## Manager resources

You might have seen posts in the #whats-happening-at-gitlab in September from the L&D team about LinkedIn Wellness Week. These posts highlighted a few short LinkedIn Learning videos that might help break up your day and re-focus your attention on your work and your surroundings.

Managers can consider re-sharing theses short videos this week in their team Slack channels!

| Post Topic | Video Links |
| ----- | ----- |
| ![post 1 image](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q3/mental-health-1.png) | [three mindful breaths](https://www.linkedin.com/learning/mindful-meditations-for-work-and-life/three-breaths-practice?u=2255073), 2 minutes, and [preparing yourself for mental agility](https://www.linkedin.com/learning/cultivating-mental-agility/physically-preparing-yourself-for-mental-agility-2?u=2255073), 4 minutes |
| ![post 3 image](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q3/mental-health-3.png) | [Simple shifts for sleep success](https://www.linkedin.com/learning/sleep-is-your-superpower/simple-shifts-for-sleep-success), 4 min and [Relax your brain](https://www.linkedin.com/learning/creativity-tips-for-all-weekly/relax-your-brain), 5 min |
| ![post 4 image](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q3/mental-health-4.png) | [desk hip stretches](https://www.linkedin.com/learning/chair-work-yoga-fitness-and-stretching-at-your-desk/hip-stretches), 5 min and [seated mountain pose](https://www.linkedin.com/learning/chair-work-yoga-fitness-and-stretching-at-your-desk/seated-mountain), 3 min  |

+(post 2 has been omitted as it was a time-sensitive webinar event)


## Discussion 

If you would like to discuss items related to this newsletter, please see the related [issue](https://gitlab.com/gitlab-com/people-group/learning-development/mental-health/-/issues/3). 
